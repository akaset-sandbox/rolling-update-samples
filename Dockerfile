FROM java:8
RUN apt-get update; apt-get install -y original-awk netcat && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*
ADD ./target/scala-2.11/spark-sample.jar ./spark-sample.jar
CMD [ "java", "-Xms512m", "-Xmx1024m", "-jar", "spark-sample.jar"]
