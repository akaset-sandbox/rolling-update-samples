package Go_files

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func main() {
	r := gin.Default()
	r.GET("/hello/:id", func(c *gin.Context) {
		id := c.Param("id")
		c.String(http.StatusOK, "I updated %s", id)
	})
	r.GET("/shutdown", func(c *gin.Context) {
		c.String(http.StatusOK, "Finish Gin.")
	})
	r.Run() // listen and serve on 0.0.0.0:8080
}
