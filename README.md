# rollingupdate-確認


## rolling update

現在の主流は`deployment`リソースを使ったものらしい
イメージの入れ替えだけをコマンドでやるパターンと, 設定ファイルapplyでやるパターンを確認してみる

## Webサーバでk8sの動作確認

デプロイ用のコード, goのwebサーバで簡易テスト(Go-files以下を参照)
```go
package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func main() {
	r := gin.Default()
	r.GET("/hello/:id", func(c *gin.Context) {
		id := c.Param("id")
		c.String(http.StatusOK, "hello %s", id)
	})
	r.Run()
}
```

Dockerfile
```dockerfile
FROM ubuntu:latest
ADD ./main main
CMD ./main
```

以下のコマンドでビルド
```bash
// linux向けにビルド
$ GOOS=linux GOARCH=amd64 go build main.go

// dockerのビルドをして,hubにあげるようにタグつけ
$ docker build . -t akaset/gin-sample:1.0

// docker-hubにログイン
$ docker login

// イメージアップロード
$ docker push akaset/gin-sample:1.0
```

で、deploymentをアプライした後下で絶えずアクセス状態にして、rolling updateをしてみる

```sh
#!/bin/sh
  i=0
  while [ $i -ne 6000 ]
  do
    i=`expr $i + 1`
    curl "<IP>:<port>/hello/${i}"
  done
```

## ローリングアップデート(イメージのみ変更) (1.1 → 1.2)

```
$ kubectl set image deployment.v1.apps/gin-sample-deployment gin-sample=docker.io/akaset/gin-sample:1.2 --record
```

検証用のshellの結果
```
llo 1089Hello 1090Hello 1091Hello 1092Hello 1093Hello 1094Hello 1095Hello 1096Hello 1097Hello 1098Hello 1099Hello 1100Hello 1101Hello 1102Hello 1103Hello 1104Hello 1105Hello 1106Hello 1107Hello 1108Hello 1109Hello 1110Hello 1111Hello 1112Hello 1113Hello 1114Hello 1115Hello 1116Hello 1117Hello 1118Hello 1119Hello 1120Hello 1121Hello 1122Hello 1123Hello 1124Hello 1125Hello 1126Hello 1127Hello 1128Hello 1129Hello 1130Hello 1131Hello 1132Hello 1133Hello 1134Hello 1135Hello 1136Hello 1137Hello 1138Hello 1139Hello 1140Hello 1141Hello 1142Hello 1143Hello 1144Hello 1145Hello 1146Hello 1147Hello 1148Hello 1149Hello 1150I updated 1151I updated 1152I updated 1153I updated 1154I updated 1155I updated 1156I updated 1157I updated 1158I updated 1159I updated 1160I updated 1161I updated 1162I updated 1163I updated 1164I updated 1165I updated 1166I updated 1167I updated 1168I
```

リクエストは捌けてる。(1149→1150で途切れていない)が、rollingupdate時に少し止まる感じがあった

## ロールバックしてみる

```
$ kubectl rollout history -f ../deployment.yaml
deployment.apps/gin-sample-deployment
REVISION  CHANGE-CAUSE
1         <none>
2         kubectl set image deployment.v1.apps/gin-sample-deployment gin-sample=docker.io/akaset/gin-sample:1.2 --record=true
```

deploymentは何世代か過去のバージョンもほじするらしい. rollbackしてみる
```
$ kubectl rollout undo deployment gin-sample-deployment --to-revision=1
```

スクリプトの感じ, こっちは比較的スムーズに更新される感じがした
```
ated 78I updated 79I updated 80I updated 81I updated 82I updated 83I updated 84I updated 85I updated 86I updated 87I updated 88I updated 89I updated 90I updated 91I updated 92I updated 93I updated 94I updated 95I updated 96I updated 97I updated 98I updated 99I updated 100I updated 101I updated 102I updated 103I updated 104I updated 105I updated 106I updated 107I updated 108Hello 109I updated 110Hello 111Hello 112Hello 113Hello 114Hello 115Hello 116Hello 117Hello 118Hello 119Hello 120Hello 121Hello 122Hello 123Hello 124Hello 125Hello 126Hello 127Hello 128Hello 129Hello 130Hello 131Hello 132Hello 133Hello 134Hello 135Hello 136Hello 137Hello 138Hello 139Hello 140Hello 141Hello 142Hello 143Hello 144Hello 145Hello 146Hello 147Hello 148Hello 149Hello 150Hello 151Hello 152Hello 153Hello 154Hello 155Hello 156Hello 157Hello 158Hello 159Hello 160Hello 161Hello 162Hello 163Hello 164Hello 165Hello 166Hello 167Hello 168Hello 169Hello 170Hello 171Hello 172Hell
```

履歴も以下のようになっている.
```
$ kubectl rollout history deployment
deployment.extensions/gin-sample-deployment
REVISION  CHANGE-CAUSE
2         kubectl set image deployment.v1.apps/gin-sample-deployment gin-sample=docker.io/akaset/gin-sample:1.2 --record=true
3         <none>
```

## preStopを確認

goのファイルに確認用のエンドポイントを用意
```go
	r.GET("/shutdown", func(c *gin.Context) {
		c.String(http.StatusOK, "Finish Gin.")
	})
```
podのライフサイクルにpreStopを追加
```yaml
    spec:
      containers:
      - name: gin-sample
        image: docker.io/akaset/gin-sample:1.1
        ports:
        - containerPort: 8080
        lifecycle:
          preStop:
            exec:
              command: ["sh", "-c", "sleep 3; curl localhost:8080/shutdown ; sleep 3"]
```

これで同様にrolling updateすると、古いほうのpodのログがこうなっている
```
[GIN] 2018/10/22 - 03:39:20 | 200 |      30.592µs |      100.96.1.1 | GET      /hello/917
[GIN] 2018/10/22 - 03:39:20 | 200 |      55.291µs |      100.96.1.1 | GET      /hello/918
[GIN] 2018/10/22 - 03:39:20 | 200 |      28.609µs |      100.96.1.1 | GET      /hello/919
[GIN] 2018/10/22 - 03:39:20 | 200 |       30.78µs |      100.96.1.1 | GET      /hello/921
[GIN] 2018/10/22 - 03:39:23 | 200 |      14.028µs |             ::1 | GET      /shutdown
```
最後にshutdownというパスが呼ばれているのがわかる
この後30秒ほど(デフォの削除時間)でpod消えるよう, preStop処理中でも強制的に消されるようなので, 
`terminationGracePeriodSeconds`オプションで安全な時間を確保すべきと思う
(sleep時間を100びょうにして、terminationGracePeriodSecondsを200にしてpreStopないの処理が実行されること確認)

挙動的には想定していた通り、 k8sのClusterIPから新しいコネクションが貼られなくなる
preStop処理を実行後、古いpodを削除する
という動きになっていそう

----

# sparkのgracefulと組み合わせ確認

上でrolling updateは確認したので、gracefulshutdownできるかだけ確認

適用
```bash
$ kubectl apply -f deployment.yaml
```

pod確認して、アクセスして、nc叩く
```bash
$ kubectl get po
NAME                                       READY   STATUS    RESTARTS   AGE
spark-sample-deployment-686c59c45b-zfcmb   1/1     Running   0          4m

$ kubectl exec -it spark-sample-deployment-686c59c45b-zfcmb bash
root@spark-sample-deployment-686c59c45b-zfcmb:/# nc -kl -p 9999
// なんか適当な文字を打っておく
```

ローリングアップデート
```bash
$ kubectl set image deployment.v1.apps/spark-sample-deployment spark-sample=docker.io/akaset/spark-sample:1.2 --record
```

あたらしいイメージが立ち上がった後に、garacefulshutdownが実行される.

イメージ
![docker-s3](https://user-images.githubusercontent.com/19891114/47312809-51b47280-d678-11e8-9ca4-d5484c64cd2b.gif)